package com.lecz.android.tiltmazes;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Connections {

    class ConnectionTime {
        public Date connectedTime;
        public Date disconnectedTime;

        public ConnectionTime() {
            // set the connected Time and disconnected Time
        }

        public void setConnectedTime() {
            connectedTime = new Date();
        }

        public long getConnectedTime() {
            if (connectedTime != null)
                return connectedTime.getTime();
            else
                return 0;
        }

        public void setDisconnectedTime() {
            disconnectedTime = new Date();
        }

        public long getDisconnectedTime() {
            if (disconnectedTime != null)
                return disconnectedTime.getTime();
            else
                return 0;
        }
    }

    private ConnectionTime connectionTime;
    private Wifi wifi;
    private FirebaseDatabase db;
    private final String connectionKey;

    public Connections(Context context) {
        db = FirebaseDatabase.getInstance();
        connectionTime = new ConnectionTime();
        wifi = new Wifi(context);
        connectionKey = "devices/connections/" + wifi.getMAC() + "/" + wifi.getBSSID();
    }

    public boolean isNewConnection() {
        return true;
    }

    /**
     Add new connection to the wifi
     1. create wifi object and save it
     2. update the connections model in db
     3. update the path in db
     */
    public void addNewConnection() {

        if(wifi.isConnected()) {
            wifi.saveWifi();

            this.connectionTime.setConnectedTime();
            DatabaseReference dbRef = db.getReference(connectionKey);
            String key = dbRef.push().getKey();

            Map<String, Object> cTime = new HashMap();
            cTime.put(key, this.connectionTime);
            dbRef.updateChildren(cTime);
        }

    }

    /**
     Disconnect the last connected wifi
     */
    public void disconnectOldConnection() {

    }

}
