package com.lecz.android.tiltmazes;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Sensor;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.Date;
import java.util.ArrayList;

public class ScanAcc extends JobService {

    private SensorManager sensorManager;
    private Sensor accelerometer;

    final DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("devices/" + UUID.randomUUID().toString() + "/accelerometer");

    /* Event Listener that checks for sensor value */
    private SensorEventListener accelerometerListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            ArrayList<String> eventList = new ArrayList();
            eventList.add(String.valueOf(event.values[0]));
            eventList.add(String.valueOf(event.values[1]));
            eventList.add(String.valueOf(event.values[2]));

            Map<String, Object> eventData = new HashMap();
            eventData.put(String.valueOf(new Date().getTime()), eventList);
            saveAccelerometerData(eventData);
        }

        /* Not-implemented */
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

        /* Job start - register accelerometer sensor. Return true to keep the context open for sensor callback */
    @Override
    public boolean onStartJob(JobParameters job) {
        sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this.accelerometerListener, accelerometer,  SensorManager.SENSOR_DELAY_NORMAL);
        Log.e("accelerometer","Accelerometer scan started");
        return true;
    }

    /* Job stop - unregister accelerometer sensor - when the job is abruptly stopped*/
    @Override
    public boolean onStopJob(JobParameters job) {
        sensorManager.unregisterListener(this.accelerometerListener);
        return false;
    }

    /* Save the data to firebaseDb */
    private void saveAccelerometerData(Map data){
        dbRef.updateChildren(data);
    }
}
