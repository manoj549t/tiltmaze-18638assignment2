package com.lecz.android.tiltmazes;

import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public final class Wifi {
    private String bssid;
    private String ssid;
    private String mac;
    private int rssi;
    private boolean connected;
    private FirebaseDatabase db;

    private Wifi(){}

    public Wifi(String bssid, String ssid) {
        this.bssid = bssid;
        this.ssid = ssid;
    }
    public Wifi(Context context) {
        this.bssid = "";
        this.ssid  = "";
        this.mac   = "";
        this.connected = false;

        this.setWifi(context);
        db = FirebaseDatabase.getInstance();
    }

    public String getBSSID() {
        return this.bssid;
    }

    public String getSSID() {
        return this.ssid;
    }

    public String getMAC() {
        return this.mac;
    }

    public int getRSSI() {
        return this.rssi;
    }

    public boolean isConnected() {
        return this.connected;
    }

    private void setWifi(Context context){
        WifiManager wifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);

        if(wifiManager.isWifiEnabled()) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();

            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());

                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    connected = true;
                    ssid  = wifiInfo.getSSID();
                    bssid = wifiInfo.getBSSID();
                    mac   = wifiInfo.getMacAddress();
                    rssi  = wifiInfo.getRssi();
                }
            }
        }
    }

    public void saveWifi() {
        try {
            DatabaseReference dbRef = db.getReference("devices/wifi/");
            Map<String, Object> wifiInstance = new HashMap();
            wifiInstance.put(this.getMAC(), this);
            dbRef.updateChildren(wifiInstance);
        } catch(Exception e) {
        }
    }

    public void addToPath() {
        try {
            DatabaseReference dbRef = db.getReference("devices/path/" + this.getMAC());
            String key = dbRef.push().getKey();

            Map<String, Object> wifiInstance = new HashMap();
            wifiInstance.put(key, this);
            dbRef.updateChildren(wifiInstance);

        } catch(Exception e) {
        }
    }
}
