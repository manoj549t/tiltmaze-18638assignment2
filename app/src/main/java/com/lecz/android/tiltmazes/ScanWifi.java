package com.lecz.android.tiltmazes;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.net.wifi.WifiManager;
import android.net.wifi.ScanResult;
import android.support.annotation.NonNull;

import android.util.Log;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Date;

public class ScanWifi extends JobService {

    private WifiManager wifiManager;

    private BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            boolean success = intent.getBooleanExtra(
                    WifiManager.EXTRA_RESULTS_UPDATED, false);
            if (!success) {
                Log.e("wifiScan","scan successful");
                scanSuccess();
            } else {
                // scan failure handling
                Log.e("wifiScan","scan failed");
                scanFailure();
            }
        }
    };
    JobParameters jobParams;

    @Override
    public boolean onStartJob(JobParameters params) {
        wifiManager = (WifiManager) this.getSystemService(this.WIFI_SERVICE);
        jobParams = params;

        try {
            if(wifiManager.isWifiEnabled()) {
                DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("devices/wifi/" + wifiManager.getConnectionInfo().getMacAddress());
                dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.exists()) {
                            Wifi currentWifi = dataSnapshot.getValue(Wifi.class);
                            Log.e("wifi","Query value exists " + dataSnapshot.toString());
                            Log.e("wifi","BSSID: " + currentWifi.getBSSID() + "\nSSID: " + currentWifi.getSSID());
                            Log.e("wifi","BSSID: " + wifiManager.getConnectionInfo().getBSSID() + "\nSSID: " + wifiManager.getConnectionInfo().getSSID());

                            if((currentWifi.getBSSID().equals(wifiManager.getConnectionInfo().getBSSID())) &&
                                    currentWifi.getSSID().equals(wifiManager.getConnectionInfo().getSSID())) {
                                Log.e("wifi","Wifi is the same");
                                // if current wifi is the same
                            }
                            else{
                                Log.e("wifi","Wifi has changed since last time");
                                // update the previous connection
                                updateConnectedWifi();
                            }

                        } else {
                            Log.e("wifi","Query value does not exist " + dataSnapshot.toString());
                            updateConnectedWifi();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        //Todo
                    }
                });
            }

            addScannedWifiToPath();

        } catch(Exception e) {
        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        this.unregisterReceiver(wifiScanReceiver);
        return false;
    }

    private void updateConnectedWifi() {
        new Connections(this).addNewConnection();
        addConnectedWifiToPath();
    }

    private void addConnectedWifiToPath(){

    }

    private void addScannedWifiToPath() {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        this.registerReceiver(wifiScanReceiver, intentFilter);

        boolean success = wifiManager.startScan();
        if (!success) {
            scanFailure();
        }

    }

    private void scanSuccess() {
        List<ScanResult> wifiResults = wifiManager.getScanResults();
        List<Wifi> wifiList = new ArrayList();
        Log.e("wifiScan", "Length wifiResults: " + wifiResults.size());

        for (ScanResult result: wifiResults) {
            Wifi wifi = new Wifi(result.BSSID, result.SSID);
            wifiList.add(wifi);
        }
        Log.e("wifiScan", "Length wifiList: " + wifiResults.size());

        Map<String, Object> scannedResults = new HashMap();
        scannedResults.put(String.valueOf(new Date().getTime()), wifiList);

        DatabaseReference newScannedEntry = FirebaseDatabase.getInstance().getReference("devices/path/" + wifiManager.getConnectionInfo().getMacAddress() + "/scanned");
        newScannedEntry.updateChildren(scannedResults);

        this.unregisterReceiver(wifiScanReceiver);
        super.jobFinished(jobParams, false);
    }

    private void scanFailure() {
        this.unregisterReceiver(wifiScanReceiver);
        super.jobFinished(jobParams, false);
        // Todo
    }

}
